package beans;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="verificacao")
@SessionScoped
public class verificacao implements Serializable{
	public String nome;
	public String senha;
	public boolean logado = false;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String action() {
		logado = true;
		return "index2.xhtml/faces-redirect=true";
	}
	
}
