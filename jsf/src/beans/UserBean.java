package beans;
import javax.faces.bean.ManagedBean;
import controle.ControleUsuario;
import modelo.Usuario;
import java.util.ArrayList;

// Dando nome ao javaBean
@ManagedBean(name="UserBean")

public class UserBean {
	public String nome;
	public String senha;
	public String email;
	public int id;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}		
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	// Lista dos usu�rios que obtida atrav�s da classe controle
	ArrayList<Usuario> lista = new ControleUsuario().consultarTodos();

	public ArrayList<Usuario> getLista() {
		return lista;
	}

	public void setLista(ArrayList<Usuario> lista) {
		this.lista = lista;
	}

	public Usuario retornarItem(int id) {
		return lista.get(id);
	}
	public void carregarId(int id) {
		Usuario user = new ControleUsuario().consultaUm(id);
		this.setNome(user.getNome());
		this.setSenha(user.getSenha());
		this.setEmail(user.getEmail());
		this.setId(user.getId());			
	}
	public String add() {
		Usuario user = new Usuario(this.getId(),this.getNome(), this.getSenha(), this.getEmail());
		if(new ControleUsuario().inserir(user)) {
			return "index";
		}else {
			return "addUser";
		}		
	}
	public String editar() {
		Usuario user = new Usuario(this.getId(),this.getNome(),this.getSenha(), this.getEmail());
		if(new ControleUsuario().editar(user)) {
			return "index";	
		}else {
			return "editar";
		}
	}
	public String deletar(int id) {
		new ControleUsuario().remover(id);
		return "index";
	}
}
