package beans;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.servlet.http.Part;
import modelo.CarrosAntigos;
import controle.ControleCAntigos;

@ManagedBean(name="CABean")
public class CABean {
	public String nome;
	public String data;
	public Part foto;
	public String descricao;
	public int id;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}	
	public Part getFoto() {
		return foto;
	}
	public void setFoto(Part foto) {
		this.foto = foto;
	}


	ArrayList<CarrosAntigos> lista = new ControleCAntigos().consultarTodos();
	public ArrayList<CarrosAntigos> getLista(){
		return lista;
	}
	public void setLista(ArrayList<CarrosAntigos> lista) {
		this.lista = lista;
	}
	public CarrosAntigos retornarItem(int id) {
		return lista.get(id);
	}
	public void carregarId(int id) {
		CarrosAntigos ca = new ControleCAntigos().consultaUm(id);
		this.setData(ca.getData());
		this.setDescricao(ca.getDescricao());
		this.setNome(ca.getNome());
		this.setId(ca.getId());		
	}
	public String add() throws IOException{
		String pasta = "D:\\eclipse-jee-2018-12-R-win32-x86_64\\jsf\\WebContent\\imgCA";
		InputStream img = foto.getInputStream();
		String nome = foto.getSubmittedFileName();
		Files.copy(img, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
		String dirImg = "imgCA/"+""+nome;		
		CarrosAntigos caa = new CarrosAntigos(this.getNome(), this.getData(),this.getDescricao(),this.getId(), dirImg);
		if(new ControleCAntigos().inserir(caa)) {
			return "home";
		}else {
			return "index2";
		}
	}
	public String deletar(int id) {
		new ControleCAntigos().remover(id);
		return "home";
	}
}
