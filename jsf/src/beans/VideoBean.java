package beans;

import modelo.Video;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.servlet.http.Part;
import controle.ControleVideo;
@ManagedBean (name="VideoBean")
public class VideoBean {
	public Part video;
	public int id;
	
	public Part getVideo() {
		return video;
	}
	public void setVideo(Part video) {
		this.video = video;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	ArrayList<Video> lista = new ControleVideo().consultarTodos();
	
	public ArrayList<Video> getLista(){
		return lista;
	}
	public void setLista(ArrayList<Video> lista) {
		this.lista = lista;
	}
	public String pegarVideo() throws IOException{
		Video videoo =  new Video();
		ControleVideo con = new ControleVideo();
		String pasta = "D:\\eclipse-jee-2018-12-R-win32-x86_64\\jsf\\WebContent\\Video";
		InputStream vid = video.getInputStream();
		String nome = video.getSubmittedFileName();
		Files.copy(vid, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
		String dirVid = "Video/"+""+ nome;
		videoo.setVideo(dirVid);
		con.inserir(videoo);
		return "video";
	}
}
