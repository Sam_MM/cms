package beans;
import modelo.Imagem;
import controle.ControleImagem;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.servlet.http.Part;

/**
 * @param id
 * @param imagem 
 * @author samira
 */
@ManagedBean (name="ImagemBean")
public class ImagemBean {	
	private int id;	
	public Part foto;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Part getFoto() {
		return foto;
	}
	public void setFoto(Part foto) {
		this.foto = foto;
	}

	ArrayList<Imagem> lista = new ControleImagem().consultarTodos();

	public ArrayList<Imagem> getLista() {
		/*
		 * Seleciona todas as imagens adicionadas pelo usuario em uma lista
		 */
		return lista;
	}

	public void setLista(ArrayList<Imagem> lista) {
		this.lista = lista;
	}	
	public String pegarImagem() throws IOException{		
		Imagem image = new Imagem();
		ControleImagem con = new ControleImagem();
		String pasta = "D:\\eclipse-jee-2018-12-R-win32-x86_64\\jsf\\WebContent\\imageUser";
		InputStream img = foto.getInputStream();
		String nome = foto.getSubmittedFileName();
		Files.copy(img, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
		String dirImg = "imageUser/"+""+nome;		
		image.setFoto(dirImg);
		con.inserir(image);
		return "foto";
	}
	public String deletar(int id) {
		new ControleImagem().remover(id);
		return "home";
	}
};
