package filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.verificacao;

@WebFilter("/Filters")
public class Filters implements Filter {
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req= (HttpServletRequest) request;
		HttpServletResponse resp= (HttpServletResponse) response;
		verificacao session=(verificacao) req.getSession().getAttribute("verificacao");
		String url= req.getRequestURI();
		if(session == null || !session.logado) {
			if(url.indexOf("index3.xhtml")>= 0 || url.indexOf("logout.xhtml")>= 0) {
				 resp.sendRedirect(req.getServletContext().getContextPath()+ "login.xhtml");
			}else {
				chain.doFilter(request, response);
			}
			
		}else {
			if(url.indexOf("cadastro.xhtml")>= 0 || url.indexOf("login.xhtml")>= 0) {
				resp.sendRedirect(req.getServletContext().getContextPath()+ "index3.xhtml");
			}else if(url.indexOf("logout.xhtml")>= 0){
				req.getSession().removeAttribute("verificacao");
				resp.sendRedirect(req.getServletContext().getContextPath()+ "login.xhtml");
			}else {
				chain.doFilter(request, response);
			}
		}
	}	
	public void init(FilterConfig fConfig) throws ServletException {
	
	}

}
