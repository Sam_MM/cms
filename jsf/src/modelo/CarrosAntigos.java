package modelo;

public class CarrosAntigos {
	public String nome;
	public String data;	
	public String descricao;
	public String foto;
	public int id;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public CarrosAntigos(String nome, String data, String descricao, int id, String foto) {
		this.setId(id);
		this.setNome(nome);
		this.setData(data);
		this.setDescricao(descricao);
		this.setFoto(foto);
	}
	
}
