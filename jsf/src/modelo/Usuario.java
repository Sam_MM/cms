package modelo;
import javax.faces.bean.ManagedBean;
/**
 * 
 *@author Samira
 *
 */
@ManagedBean
public class Usuario {
	private String nome;
	private String senha;
	private String email;	
	private int id;

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}	
	public String getSenha() {
		return senha;
	}	
	public void setSenha(String senha) {
		this.senha = senha;
	}	
	public String getEmail() {
		return email;
	}	
	public void setEmail(String email) {
		this.email = email;
	}	
	public int getId() {
		return id;
	}	
	public void setId(int id) {
		this.id = id;
	}
	public Usuario(int id, String nome, String senha, String email) {
		this.setId(id);
		this.setNome(nome);
		this.setSenha(senha);
		this.setEmail(email);
	}
	
}
