package chave;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class MyCript {
	public String cifrar(String texto) throws SQLException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
		  String resultado = null;
		  Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		// Inicializa a cifra para o processo de encriptação
		  byte[]  mensagem = texto.getBytes(); 
		   byte[] chave = "Chave de 16bytes".getBytes();
		  try {
			  cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(chave, "AES"));  
		  }catch(InvalidKeyException e) {
			  e.printStackTrace();
		  }
		  
		  byte[] encrypted = cipher.doFinal(mensagem);
		  String base = Base64.getEncoder().encodeToString(encrypted);
		  resultado = base;
		  return resultado;
	  }
	/*public static void main(String[] argv){
		try {
			KeyGenerator keygenerator = KeyGenerator.getInstance("DES");
			SecretKey chaveDES = keygenerator.generateKey();
			Cipher cifraDES;
			//cifra e cria			
			cifraDES = Cipher.getInstance("DES/EBC/PKCSSPadding");
			//inicialza a cifra para a encriptografia
			cifraDES.init(Cipher.ENCRYPT_MODE, chaveDES);
			//texto puro
			byte[] textoPuro = "exemplo de texto".getBytes();
			System.out.println("Texto puro formato byte:" + textoPuro);
			System.out.println("Texto puro:" + new String(textoPuro));
			byte[] textoEncriptografado = cifraDES.doFinal(textoPuro);
			System.out.println("Texto encriptado :" + textoEncriptografado);
			cifraDES.init(Cipher.DECRYPT_MODE, chaveDES);
			byte[] textoDecriptografado = cifraDES.doFinal(textoEncriptografado);
			System.out.println("Texto decriptografado :" + new String(textoDecriptografado));
			
		}catch(NoSuchAlgorithmException e){
			e.printStackTrace();
		}catch(NoSuchPaddingException e ) {
			e.printStackTrace();
		}catch(InvalidKeyException e) {
			e.printStackTrace();
		}catch(IllegalBlockSizeException e) {
			e.printStackTrace();
		}catch(BadPaddingException e) {
			e.printStackTrace();
		}
		}*/
		
}
