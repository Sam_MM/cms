package controle;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import controle.Conexao;
import modelo.Imagem;


public class ControleImagem {
	public boolean inserir(Imagem img){
		boolean retorno = false;
		try {			
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("INSERT INTO imagem(foto) values(?);");			
			ps.setString(1, img.getFoto());
			if(!ps.execute()) {
				retorno = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e ){
			System.out.println(e.getMessage());
		}
		return retorno;
		
	}
	public ArrayList<Imagem> consultarTodos(){ 
		ArrayList<Imagem> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM imagem");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {				
				lista = new ArrayList<Imagem>();				
				while(rs.next()) {
					Imagem img = new Imagem();
					img.setFoto(rs.getString("foto"));
					img.setId(rs.getInt("id"));
					lista.add(img);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return lista;
	}
	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM imagem WHERE id=?");
			ps.setInt(1, id);
			if(ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}

}
