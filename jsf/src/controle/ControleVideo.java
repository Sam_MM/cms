package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.Video;

public class ControleVideo {
	public boolean inserir(Video vid){
		boolean retorno = false;
		try {			
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("INSERT INTO video(video) values(?);");			
			ps.setString(1, vid.getVideo());
			if(!ps.execute()) {
				retorno = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e ){
			System.out.println(e.getMessage());
		}
		return retorno;
		
	}
	public ArrayList<Video> consultarTodos(){ 
		ArrayList<Video> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM video");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {				
				lista = new ArrayList<Video>();				
				while(rs.next()) {
					Video vid = new Video();
					vid.setVideo(rs.getString("video"));
					vid.setId(rs.getInt("id"));
					lista.add(vid);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return lista;
	}
	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM video WHERE id=?");
			ps.setInt(1, id);
			if(ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}

}
