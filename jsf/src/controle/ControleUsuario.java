package controle;
import modelo.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import controle.Conexao;
import chave.Crypt;
public class ControleUsuario {
	public boolean inserir(Usuario user) {
		boolean retorno = false;
		try {
			Crypt crip = new Crypt();
			String chave = "ghgyujkhjnkjhjuhklihljuhuih" + 
					"ghjkl�jdnljndldfkimoijijuiu" +
					"hiuhkuhgyfuftd3swkiugigigig" +
					"ghjkl�jdnljndldfkimoijijuiu" +
					"ojijhohiguyftfytdrtdrtddrtd" +
					"hiuhkuhgyfuftd3swkiugigigig" +
					"lyuguygkuykgkyftftrhrhdrdrr" +
					"io�jijupjtrdhddjdtrdhtrdrdt" +
					"ghjkl�jdnljndldfkimoijijuiu" +
					"lyuguygkuykgkyftftrhrhdrdrr" +
					"ojijhohiguyftfytdrtdrtddrtd" ;
			
			String senha = user.getSenha();
			byte [] result = crip.encrypt(senha, chave);
			Connection con = new Conexao().abrirConexao();
			String sql="INSERT INTO usuario(nome,senha,email) VALUES(?,?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user.getNome());
			ps.setBytes(2, result);
			ps.setString(3, user.getEmail());
			if(!ps.execute()) {
				retorno = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return retorno;
	}
	public Usuario logar(Usuario user) throws SQLException{
		Usuario useer = null;				
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM usuario WHERE nome=? and senha=?;");
		ps.setString(1, user.getNome());
		ps.setString(2, user.getSenha());
		ResultSet rs = ps.executeQuery();
		if(rs.next() && rs != null){
			useer = new Usuario(rs.getInt("id"),rs.getString("nome"),rs.getString("senha"),rs.getString("email"));
			useer.setId(rs.getInt("id"));
			useer.setNome(rs.getString("nome"));
			useer.setSenha(rs.getString("senha"));
		}else{
			useer=null;
		}
		new Conexao().fecharConexao(con);	
		return useer;
	}
	public ArrayList<Usuario> consultarTodos(){
		ArrayList<Usuario> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM usuario;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Usuario>();
				while(rs.next()) {
					Usuario user = new Usuario(rs.getInt("id"),rs.getString("nome"),rs.getString("senha"),rs.getString("email"));
					lista.add(user);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	public Usuario consultaUm(int id) {
		Usuario user = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM usuario WHERE id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				while(rs.next()) {
				user = new Usuario(rs.getInt("id"),rs.getString("nome"),rs.getString("senha"),rs.getString("email"));		
				}
			}
		new Conexao().fecharConexao(con);
	}catch(SQLException e) {
		System.out.println("Erro no servidor: " + e.getMessage());
	}
		
		return user;
	}
	
	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM usuario WHERE id=?");
			ps.setInt(1, id);
			if(ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}
	public boolean editar(Usuario user) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE usuario SET nome=?, senha=?, email=? WHERE id=?");
			ps.setString(1,user.getNome());
			ps.setString(2, user.getSenha());			
			ps.setString(3, user.getEmail());
			ps.setInt(4, user.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar o usu�rio: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro: "+ e.getMessage());
		}
		return resultado;
	}

	
}


