package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.CarrosAntigos;

public class ControleCAntigos {
	public boolean inserir(CarrosAntigos ca) {
		boolean retorno = false;
		try {			
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("INSERT INTO carrosAntigos(nome,data,descricao,foto) values(?,?,?,?);");
			ps.setString(1, ca.getNome());
			ps.setString(2, ca.getData());			
			ps.setString(3, ca.getDescricao());
			ps.setString(4, ca.getFoto());
			if(!ps.execute()) {
				retorno = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e ){
			System.out.println(e.getMessage());
		}
		return retorno;
	}
	public ArrayList<CarrosAntigos> consultarTodos() {
		ArrayList<CarrosAntigos> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM carrosAntigos;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<CarrosAntigos>();
				while(rs.next()) {
					CarrosAntigos ca = new CarrosAntigos(rs.getString("nome"),rs.getString("data"),rs.getString("descricao"), rs.getInt("id"),rs.getString("foto"));
					ca.setFoto(rs.getString("foto"));
					lista.add(ca);
				}
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}
	public CarrosAntigos consultaUm(int id) {
		CarrosAntigos ca = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM carrosAntigos WHERE id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					ca = new CarrosAntigos(rs.getString("nome"),rs.getString("data"),rs.getString("descricao"), rs.getInt("id"),rs.getString("foto"));
					ca.setFoto(rs.getString("foto"));
				}
			}
		new Conexao().fecharConexao(con);
	}catch(SQLException e) {
		System.out.println("Erro no servidor: " + e.getMessage());
	}
		
		return ca;
	}
	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM carrosAntigos WHERE id=?");
			ps.setInt(1, id);
			if(ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}
	public boolean editar(CarrosAntigos ca) {
		boolean resultado = false;
			try {
				Connection con = new Conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("UPDATE carrosAntigos SET nome=?, descricao=?, data=? WHERE id=?");
				ps.setString(1, ca.getNome());
				ps.setString(2, ca.getDescricao());
				ps.setString(3, ca.getData());
				ps.setInt(3, ca.getId());
				if(!ps.execute()) {
					resultado = true;
				}
				new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro ao editar"+ e.getMessage());
			}catch(Exception e) {
				System.out.println("Erro:"+ e.getMessage());
			}
		return resultado;
	}

}


